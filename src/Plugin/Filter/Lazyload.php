<?php

namespace Drupal\lazyload\Plugin\Filter;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\lazyload\LazyloadHelper;
use Drupal\linkit\SubstitutionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to lazy load images.
 *
 * @Filter(
 *   id = "filter_lazyload",
 *   title = @Translation("Lazyload"),
 *   description = @Translation("Lazyload iframes and images that do not come from the media library. This filter must be placed after embed images/media."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 10
 * )
 */
class Lazyload extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Lazy load helper service.
   *
   * @var \Drupal\lazyload\LazyloadHelper
   */
  protected LazyloadHelper $lazyloadHelper;

  /**
   * Constructs a lazy load filter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param LazyloadHelper $lazyload_helper
   *   Lazy load helper service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LazyloadHelper $lazyload_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->lazyloadHelper = $lazyload_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lazyload.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $document = Html::load($text);
    $xpath = new \DOMXPath($document);

    $elements = $xpath->query('//img|//iframe|//picture//source');
    for ($i = 0; $i < $elements->length; $i++) {
      $element = $elements->item($i);
      $this->lazyloadHelper->processDomNode($element);
    }

    return new FilterProcessResult(Html::serialize($document));
  }

}
