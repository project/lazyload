<?php

namespace Drupal\lazyload\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Global lazy load settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lazyload_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lazyload.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lazyload.settings');

    $image_styles = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->loadMultiple();

    $form['image_styles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Image styles'),
      '#default_value' => array_keys($config->get('image_styles')) ?: [],
      '#options' => array_map(fn($s) => $s->label(), $image_styles),
    ];

    $form['use_javascript'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use JavaScript'),
      '#description' => $this->t("Use JavaScript for lazy loading instead of browsers' native lazy loading."),
      '#default_value' => $config->get('use_javascript') ?: FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('lazyload.settings');

    $image_styles = array_map(
      fn() => 0,
      array_filter($form_state->getValue('image_styles'))
    );
    ksort($image_styles);

    $config->set('image_styles', $image_styles);
    $config->set('use_javascript', (bool) $form_state->getValue('use_javascript'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
