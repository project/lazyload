<?php

namespace Drupal\lazyload;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Url;

/**
 * Lazyload Helper.
 */
class LazyloadHelper {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('lazyload.settings');
  }

  /**
   * Applies lazy load to the given element.
   *
   * @param \DOMNode $node
   *   A DOM node.
   */
  public function processDomNode(\DOMNode $node): void {
    if (!$this->useJavaScript()) {
      $node->setAttribute('loading', 'lazy');
      return;
    }

    switch ($node->nodeName) {
      case 'iframe':
        $node->setAttribute('data-lazyload-src', $node->getAttribute('src'));
        $node->setAttribute('src', 'about:blank');
        break;

      case 'img':
        $node->setAttribute('data-lazyload-src', $node->getAttribute('src'));
        $node->setAttribute('src', $this->getDotGifPath());
        break;

      case 'source':
        $node->setAttribute('data-lazyload-srcset', $node->getAttribute('srcset'));
        $node->setAttribute('srcset', $this->getDotGifPath() . ' 1x');
        break;

      default:
        var_dump($node->nodeName);
        exit;
    }
  }

  /**
   * Applies lazy load to an image renderable array.
   *
   * @param array $image
   *   A renderable array for an image.
   */
  public function processImageArray(array &$image): void {
    if (!$this->isImageStyleLazy($image['style_name'] ?? '')) {
      unset($image['attributes']['loading']);
      return;
    }

    if (!$this->useJavaScript()) {
      $image['attributes']['loading'] = 'lazy';
      return;
    }

    $image['attributes']['data-lazyload-src'] = $image['attributes']['src'];
    $image['attributes']['src'] = $this->getDotGifPath();
  }

  /**
   * Preprocess a page renderable array.
   *
   * @param array $page
   *   A renderable array for a page.
   */
  public function processPageArray(array &$page): void {
    if (!$this->useJavaScript()) {
      return;
    }

    $page['#attached']['library'][] = 'lazyload/lazyload';
  }

  /**
   * Get whether an image style is lazy-loaded or not.
   *
   * @param string $id
   *   An image style ID.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isImageStyleLazy(string $id): bool {
    return array_key_exists($id ?? NULL, $this->config->get('image_styles'));
  }

  private function getDotGifPath(): string {
    $base_path = Url::fromRoute('<front>', [])->toString();
    $gif_path = \Drupal::service('module_handler')->getModule('lazyload')->getPath()
      . '/assets/dot.gif';

    return $base_path . $gif_path;
  }

  /**
   * Whether to use JavaScript for lazy loading.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function useJavaScript(): bool {
    return (bool) $this->config->get('use_javascript');
  }

}
