Drupal.lazyload = Drupal.lazyload || {};

/**
 * Reveals a lazy-loaded element.
 */
Drupal.lazyload.reveal = function (el) {
  if (el.dataset.lazyloadSrcset) {
    el.setAttribute('srcset', el.dataset.lazyloadSrcset);
    delete el.dataset.lazyloadSrcset;
    return;
  }

  el.setAttribute('src', el.dataset.lazyloadSrc);
  delete el.dataset.lazyloadSrc;
}

/**
 * Handles intersection of progress objects.
 */
Drupal.lazyload.intersectionHandler = (entries, observer) => {
  entries.forEach((entry) => {
    if (!entry.isIntersecting) {
      return;
    }

    Drupal.lazyload.intersectionObserver.unobserve(entry.target);
    Drupal.lazyload.reveal(entry.target);
  });
};

/**
 * Intersection Observer for lazy loading.
 *
 * @type {IntersectionObserver}
 */
Drupal.lazyload.intersectionObserver = window['IntersectionObserver']
  ? new IntersectionObserver(Drupal.lazyload.intersectionHandler, {})
  : null;

/**
 * Lazyload behaviors.
 *
 * @type {Drupal~behavior}
 */
Drupal.behaviors.lazyload = {
  attach: function(context, settings) {
    document.querySelectorAll('[data-lazyload-src], [data-lazyload-srcset]')
      .forEach(function (el) {
        if (el._lazyloadInitialized) {
          return;
        }

        el._lazyloadInitialized = true;

        if (Drupal.lazyload.intersectionObserver) {
          Drupal.lazyload.intersectionObserver.observe(el);
          return;
        }

        // The browser doesn't have intersection observers, so just load.
        Drupal.lazyload.reveal(el);
      });
  },
  detach: function() {

  }
}
