# Lazyload: Light-weight Lazy-loading

This module allows the admin to enable lazy-loading for images and iframes.

## How it works?

  * Install and enable the module like any other Drupal module.
  * From the `/admin/config/media/lazyload` page,
    * Select the image styles that should be lazy-loaded.
    * Select whether JavaScript should be used for lazy loading. This prevents
      browsers like Chrome to eagerly load resources.
  * Lazy-loading for images/iframes in Rich Text Editor (RTE) content can be
    enabled by enabling the *Lazyload* filter for the text format.
